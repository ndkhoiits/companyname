package com.trungnt.cpn.logic;

import com.trungnt.cpn.model.Rule;

public abstract class CommonRule {
    protected Rule rule;
    protected static final String L_PAREN = "(";
    protected static final String R_PAREN = ")";
    protected static final String L_BRACE = "{";
    protected static final String R_BRACE = "}";
    protected static final String L_BRACKET = "[";
    protected static final String R_BRACKET = "]";

    public CommonRule(Rule rule) {
        this.rule = rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public abstract String process(String input);
}
