package com.trungnt.cpn.logic;

import com.trungnt.cpn.model.Rule;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KeepRule extends CommonRule {

    public KeepRule(Rule rule) {
        super(rule);
    }

    @Override
    public String process(String input) {
        String result = "";
        List<String> indicators = rule.getIndicators();
        String subType = rule.getSubtype();
        String regex = "";
        if (rule.getSubtype().equalsIgnoreCase("custom")) {
            for (String i : indicators) {
                input = match(i, input);
            }
        }
        result = input;
        return result;
    }

    private String match(String regex, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        String result = input;
        if (matcher.find()) {
            result = matcher.group(1);
        }
        return result;
    }
}
