package com.trungnt.cpn.logic;

import com.trungnt.cpn.model.Rule;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DeleteRule extends CommonRule {
    public DeleteRule(Rule rule) {
        super(rule);
    }

    public String process(String input) {
        String result = "";
        List<String> indicators = rule.getIndicators();
        String subType = rule.getSubtype();
        String regex = "";
        String exclude = rule.getExclude();
        if (rule.getSubtype().equalsIgnoreCase("endwith")) {
            for (String indicator : indicators) {
                regex = "\\s" + indicator + "$";
                if (exclude != null) {
                    input = removeText(regex, exclude, input);
                } else {
                    input = removeText(regex, input);
                }
            }
        } else if (subType.equalsIgnoreCase("inside")) {
            for (String indicator : indicators) {
                if (indicator.equals(L_PAREN)) {
                    regex = "\\" + L_PAREN + ".*" + "\\" + R_PAREN;
                } else if (indicator.equals(L_BRACKET)) {
                    regex = "\\" + L_BRACKET + ".*" + "\\" + R_BRACKET;
                } else if (indicator.equals(L_BRACE)) {
                    regex = "\\" + L_BRACE + ".*" + "\\" + R_BRACE;
                }
                if (exclude != null) {
                    input = removeText(regex, exclude, input);
                } else {
                    input = removeText(regex, input);
                }
            }
        } else if (subType.equalsIgnoreCase("after")) {
            for (String i : indicators) {
                regex = i + ".*$";
                input = removeText(regex, exclude, input);
            }
        } else if (subType.equalsIgnoreCase("between")) {
            for (String i : indicators) {
                String[] groups = i.split("\\|\\|");
                regex = groups[0] + ".*" + groups[1];
                input = removeText(regex, input);
            }
        } else if (subType.equalsIgnoreCase("custom")) {
            for (String i : indicators) {
                input = removeText(i, exclude, input);
            }
        }

        result = input;
        return result;
    }

    private String removeText(String regex, String exclude, String input) {
        if (exclude == null) {
            return removeText(regex, input);
        } else {
            Pattern pattern = Pattern.compile(regex);
            Pattern excludePattern = Pattern.compile(exclude);

            Matcher matcher = pattern.matcher(input);
            Matcher excludeMatcher = excludePattern.matcher(input);
            String result = input;
            if (matcher.find() && excludeMatcher.find()) {
                String search = matcher.group();
                String excludeSearch = excludeMatcher.group();
                if (excludeSearch.contains(search)) {
                    return result;
                }
            }
            result = matcher.replaceAll("");
            return result;

        }
    }

    private String removeText(String regex, String input) {
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        String result = input;
        result = matcher.replaceAll("");
        return result;
    }
}
