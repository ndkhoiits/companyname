package com.trungnt.cpn.logic;

import com.trungnt.cpn.model.Rule;

public class Latinized extends CommonRule {

    public Latinized(Rule rule) {
        super(rule);
    }

    @Override
    public String process(String input) {
        input = input.replaceAll("[^\\u0000-\\u007F]", "");
        return input;
    }
}
