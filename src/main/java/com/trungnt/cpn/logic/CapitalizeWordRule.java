package com.trungnt.cpn.logic;

import com.trungnt.cpn.model.Rule;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CapitalizeWordRule extends CommonRule {
    public CapitalizeWordRule(Rule rule) {
        super(rule);
    }

    @Override
    public String process(String input) {
        String result = input;
        input = input.trim();
        String exclude = rule.getExclude();
        Pattern excludePattern = Pattern.compile(exclude);
        Matcher matcher = excludePattern.matcher(input);
        if (matcher.find()) {
            return result;
        }
        String[] words = input.split("\\s");
        if (words.length > 1 && input.toUpperCase().equals(input)) {
            input = input.toLowerCase();
        }
        input = capitalize(input.trim());
        result = input;
        return result;
    }

    private String capitalize(String str) {
        StringBuilder result = new StringBuilder();
        String[] words = str.split("\\s");
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (i > 0) {
                result.append(" ");
            }

            if (word.length() > 1) {
                result.append(Character.toUpperCase(word.charAt(0))).append(word.substring(1));
            } else {
                result.append(word);
            }

        }
        return result.toString();
    }
}
