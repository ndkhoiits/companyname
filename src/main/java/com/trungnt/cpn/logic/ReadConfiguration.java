package com.trungnt.cpn.logic;

import com.trungnt.cpn.model.ApplicationConfig;
import com.trungnt.cpn.model.Rule;
import com.trungnt.cpn.utils.Constants;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class ReadConfiguration {
    public static List<Rule> loadRules() {
        JAXBContext context = null;
        try {
            String configFile = Constants.DIST_DIR + File.separator + "conf" + File.separator + "rules-configuration.xml";
            context = JAXBContext.newInstance(ApplicationConfig.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            ApplicationConfig configuration = (ApplicationConfig) unmarshaller.unmarshal(new File(configFile));
            List<Rule> rules = configuration.getRules();
            return rules;
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return null;
    }
}
