package com.trungnt.cpn.utils;

import org.kohsuke.args4j.Option;

public class CommandLineValues {
    @Option(name = "-i", aliases = "--inputFile", required = false)
    private String inputFile;

    @Option(name = "-o", aliases = "--outputFile", required = false)
    private String outputFile;

    public String getInputFile() {
        return inputFile;
    }

    public void setInputFile(String inputFile) {
        this.inputFile = inputFile;
    }

    public String getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(String outputFile) {
        this.outputFile = outputFile;
    }
}
