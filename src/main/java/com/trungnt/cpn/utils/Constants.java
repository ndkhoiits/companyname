package com.trungnt.cpn.utils;

import java.io.File;
import java.net.URISyntaxException;

public class Constants {
    public static String DIST_DIR = getDistDir();

    private static String getDistDir() {
        String folder = null;
        try {
            File jarFile = new File(Constants.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
            folder = jarFile.getParent();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return folder;
    }

    private static String getSourceDir() {
        return "/Users/khoinguyen/working/projects/companyname/src/main/resources";
    }

}
