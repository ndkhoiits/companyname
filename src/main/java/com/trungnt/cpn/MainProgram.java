package com.trungnt.cpn;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.trungnt.cpn.logic.*;
import com.trungnt.cpn.model.CompanyName;
import com.trungnt.cpn.model.Rule;
import com.trungnt.cpn.utils.CommandLineValues;
import com.trungnt.cpn.utils.Constants;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class MainProgram {
    private static List<Rule> rules = ReadConfiguration.loadRules();

    public static void main(String[] args) throws IOException {
        String inputFile = Constants.DIST_DIR + File.separator + "input.csv";
        String output = Constants.DIST_DIR + File.separator + "output.csv";
        CommandLineValues values = new CommandLineValues();
        CmdLineParser parser = new CmdLineParser(values);
        try {
            parser.parseArgument(args);
            if (values.getInputFile() == null) {
                System.out.println("Use default input file [input.csv]");
            } else {
                inputFile = values.getInputFile();
                System.out.println("Use custom input file [" + inputFile + "]");
            }
            if (values.getOutputFile() == null) {
                System.out.println("User default output file [output.csv]");
            } else {
                output = values.getOutputFile();
                System.out.println("Use custom output file [" + output + "]");
            }
        } catch (CmdLineException e) {
            e.printStackTrace();
        }

        List<String> names = readOriginalName(inputFile);
        List<CompanyName> companyNames = new ArrayList<CompanyName>();
        for (String s : names) {
            CompanyName c = new CompanyName();
            c.setOriginalName(s.trim());
            companyNames.add(c);
        }

        for (CompanyName companyName : companyNames) {
            String refinedName = process(companyName.getOriginalName());
            companyName.setRefinedName(refinedName);
        }

        CSVWriter writer = new CSVWriter(new FileWriter(output));
        List<String[]> lines = new ArrayList<String[]>();
        for (CompanyName c : companyNames) {
            lines.add(new String[]{c.getOriginalName(), c.getRefinedName()});
        }
        writer.writeAll(lines);
        writer.flush();
    }

    public static List<String> readOriginalName(String filename) {
        List<String> result = new ArrayList<String>();
        try {
            CSVReader reader = new CSVReader(new FileReader(filename));
            List<String[]> names = reader.readAll();
            for (String[] strings : names) {
                result.add(strings[0]);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String process(String input) {
        String result = "";
        String lastResult = input;
        while (!lastResult.equals(result)) {
            lastResult = input;
            for (Rule r : rules) {
                String type = r.getType();
                if (r.getType().equalsIgnoreCase("delete")) {
                    CommonRule rule = new DeleteRule(r);
                    input = rule.process(input);
                } else if (type.equalsIgnoreCase("capitalize")) {
                    CommonRule rule = new CapitalizeWordRule(r);
                    input = rule.process(input);
                } else if (type.equalsIgnoreCase("latinized")) {
                    CommonRule rule = new Latinized(r);
                    input = rule.process(input);
                } else if (type.equalsIgnoreCase("keep")) {
                    CommonRule rule = new KeepRule(r);
                    input = rule.process(input);
                }
            }
            result = input;

//            System.out.println(result);
        }
        return result;
    }
}
