package com.trungnt.cpn.model;

public class Indicator {
    private String indicator;

    public String getIndicator() {
        return indicator;
    }

    public void setIndicator(String indicator) {
        this.indicator = indicator;
    }
}
