package com.trungnt.cpn.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "configuration")
public class ApplicationConfig {
    public ApplicationConfig() {
        rules = new ArrayList<Rule>();
    }

    private List<Rule> rules;

    @XmlElementWrapper(name = "rules")
    @XmlElement(name = "rule")
    public List<Rule> getRules() {
        return rules;
    }

    public void setRules(List<Rule> rules) {
        this.rules = rules;
    }
}
