package com.trungnt.cpn.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;
import java.util.List;

public class Rule {
    public Rule() {
        indicators = new ArrayList<String>();
    }

    public Rule(String type, String description) {
        this();
        this.type = type;
        this.description = description;
    }

    private String type;
    private String description;
    private String subtype;
    private List<String> indicators;
    private String exclude;

    public String getExclude() {
        return exclude;
    }

    public void setExclude(String exclude) {
        this.exclude = exclude;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    @XmlElementWrapper(name = "indicators")
    @XmlElement(name = "indicator")
    public List<String> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<String> indicators) {
        this.indicators = indicators;
    }
}
