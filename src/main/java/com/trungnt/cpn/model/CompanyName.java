package com.trungnt.cpn.model;

public class CompanyName {
    private String originalName;
    private String refinedName;

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getRefinedName() {
        return refinedName;
    }

    public void setRefinedName(String refinedName) {
        this.refinedName = refinedName;
    }
}
