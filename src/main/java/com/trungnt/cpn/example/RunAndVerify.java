package com.trungnt.cpn.example;

import com.trungnt.cpn.MainProgram;

import java.io.IOException;

public class RunAndVerify {
    public static void main(String[] args) throws IOException {
        MainProgram.main(args);
        Verify.main(args);
    }
}
