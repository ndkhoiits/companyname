package com.trungnt.cpn.example;

import com.trungnt.cpn.MainProgram;

/**
 * @author <a href="khoi.nguyen@niteco.se">Khoi NGUYEN</a>
 */
public class Test {
    public static void main(String[] args) {
        String input = "Eco-Futures, Inc. - Sanctuary Belize Project";
        String out = MainProgram.process(input);
        System.out.println(out);
    }
}
