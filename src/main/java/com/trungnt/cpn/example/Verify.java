package com.trungnt.cpn.example;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import com.trungnt.cpn.MainProgram;
import com.trungnt.cpn.utils.Constants;
import sun.applet.Main;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="khoi.nguyen@niteco.se">Khoi NGUYEN</a>
 */
public class Verify {
    public static void main(String[] args) throws IOException {
        Map<String, String> output = new HashMap<String, String>();
        Map<String, String> expected = new HashMap<String, String>();
        List<String[]> result = new ArrayList<String[]>();
        CSVReader out = new CSVReader(new FileReader(Constants.DIST_DIR + File.separator + "output.csv"));

        List<String[]> strs = out.readAll();
        for (String[] line : strs) {
            output.put(line[0], line[1]);
        }

        CSVReader expectReader = new CSVReader(new FileReader(System.getProperty("user.home") + "/working/projects/companyname/src/main/resources/expected.csv"));
        List<String[]> expcetes = expectReader.readAll();
        System.out.println(expcetes.size());
        for (String[] line : expcetes) {
            expected.put(line[0].trim(), line[1]);
        }

        for (String key : output.keySet()) {
            String name = output.get(key);
            String expectName = expected.get(key);
            if (!name.equals(expectName)) {
                System.out.println("Wrong at " + key + " - expected : " + expectName + " - actual:" + name);
                result.add(new String[]{key, name, expectName});
            }
        }

        if (result.size() > 0) {
            CSVWriter writer = new CSVWriter(new FileWriter(Constants.DIST_DIR + File.separator + "result.csv"));
            writer.writeAll(result);
            writer.flush();
            writer.close();
        }
    }
}
