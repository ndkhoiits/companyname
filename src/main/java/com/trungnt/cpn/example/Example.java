package com.trungnt.cpn.example;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Example {
    public static void main(String[] args) {
        String test = "Art For Everyday";
        String regex1 = "(?<!Centre) +For.*";
        Pattern p1 = Pattern.compile(regex1);

        Matcher matcher = p1.matcher(test);
        System.out.println(matcher.find());

    }
}
